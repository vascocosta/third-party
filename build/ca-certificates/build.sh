##
## Copyright (c) 2013 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the tar package.
##
## Author:
##
##     Evan Green 20-Jan-2014
##
## Environment:
##
##     Build
##

. ../build_common.sh

set -e

cd $SOURCE_DIRECTORY

##
## Remove \r from the files.
##

if [ "$BUILD_OS" != "win32" ]; then
    for f in certdata.txt make-ca.sh make-cert.pl revision; do
        cat $f | tr -d '\r' > $f.tmp
        mv $f.tmp $f
    done
fi

REVISION=`cat revision`
if ! [ -f ./ca-bundle-$REVISION ] || \
   [ ./certdata.txt -nt ./ca-bundle-$REVISION.crt ]; then

    sh ./make-ca.sh
fi

CERTS_DIRECTORY=$OUTPUT_DIRECTORY/etc/ssl/certs
rm -rf "$CERTS_DIRECTORY"
mkdir -p "$CERTS_DIRECTORY"
cp -pv ca-bundle-$REVISION.crt "$CERTS_DIRECTORY"
cp -pv revision "$OUTPUT_DIRECTORY/"
ln -sf ca-bundle-$REVISION.crt "$CERTS_DIRECTORY/ca-bundle.crt"

##
## TODO: Adopt the later make-ca.sh, de-bashify it, and make the symlinks etc in
## /etc/ssl/certs.
##

cd "$OUTPUT_DIRECTORY/etc/ssl"
ln -sf certs/ca-bundle.crt cert.pem

