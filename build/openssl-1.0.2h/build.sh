##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the OpenSSL package.
##
## Author:
##
##     Evan Green 13-Jan-2015
##
## Environment:
##
##     Minoca Build.
##

. ../build_common.sh

LIBGCC=libgcc_6.3.0

if [ x"$BUILD_OS" == "xwin32" ]; then
  export INHIBIT_SYMLINKS=yes
fi

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    export CC="$TARGET-gcc"
    extract_dependency "$LIBGCC" || echo "Ignoring libgcc."
    export CFLAGS="$CFLAGS -I$DEPENDROOT/usr/include"
    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    ##
    ## OpenSSL must be built in its source directory.
    ##

    cp -Rp ${SOURCE_DIRECTORY}/* .
    if [ "$BUILD_OS" != "minoca" ]; then
      export PERL=perl
      export SYSTEM=Minoca
      case $ARCH$VARIANT in
      x86q) export MACHINE=i586 ;;
      x86) export MACHINE=i686 ;;
      x64) export MACHINE=x86_64 ;;
      *) export MACHINE=$ARCH ;;
      esac
    fi

    sh ./config --prefix=/usr -shared --openssldir=/etc/ssl "$CFLAGS" "$LDFLAGS"
    ;;

  configure-tools)

    ##
    ## OpenSSL must be built in its source directory.
    ##

    cp -Rp ${SOURCE_DIRECTORY}/* .
    if [ x"$BUILD_OS" == "xwin32" ]; then
      export PERL=perl
    fi

    sh ./config --prefix=$OUTPUT_DIRECTORY -shared --openssldir=$OUTPUT_DIRECTORY/etc/ssl "$CFLAGS" "$LDFLAGS"
    ;;

  build)
    $MAKE
    $MAKE install INSTALL_PREFIX="$OUTPUT_DIRECTORY"

    ##
    ## Remove the man directory from the openssl directory, as it's bulky.
    ##

    rm -rf "$OUTPUT_DIRECTORY/etc/ssl/man"
    ;;

  build-tools)
    $MAKE
    rm -rf "$OUTPUT_DIRECTORY/lib/engines"
    $MAKE install
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

