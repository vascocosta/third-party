##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the harfbuzz library.
##
## Author:
##
##     Evan Green 7-Mar-2017
##
## Environment:
##
##     Build
##

. ../build_common.sh

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    extract_dependency libfreetype_2.7.1
    extract_dependency libglib_2.49.6

    extract_dependency libpcre_8.39
    extract_dependency gettext_0.19.8.1
    extract_dependency libpng_1.6.28
    extract_dependency libiconv_1.14

    export CFLAGS="$CFLAGS -I$DEPENDROOT/usr/include \
 -I$DEPENDROOT/usr/include/glib-2.0 -I$DEPENDROOT/usr/lib/glib-2.0/include"

    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    export CC="$TARGET-gcc"
    cp -Rfp ${SOURCE_DIRECTORY}/* ${BUILD_DIRECTORY}
    sh ./configure $BUILD_LINE \
                   --host="$TARGET" \
                   --target="$TARGET" \
                   --prefix="/usr" \
                   --sysconfdir=/etc \
                   --disable-static \
                   --with-gobject \
                   CFLAGS="$CFLAGS" \
                   LDFLAGS="$LDFLAGS"

    ;;

  build)
    make $PARALLEL_MAKE
    make $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

