##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the boost library.
##
## Author:
##
##     Chris Stevens 19-Jul-2016
##
## Environment:
##
##     Build
##

. ../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/lib" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/include" "$PACKAGE_DIRECTORY/usr"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: boost
Priority: optional
Version: 1_59_0
Architecture: $PACKAGE_ARCH
Maintainer: Chris Stevens <chris@minocacorp.com>
Section: main
Source: http://sourceforge.net/projects/boost/files/boost/1.59.0/boost_1_59_0.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: Boost provides portable C++ libraries.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

