##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the dmake package.
##
## Author:
##
##     Evan Green 10-May-2016
##
## Environment:
##
##     Build
##

. ../build_common.sh

##
## Export some variables needed to build correctly on Windows.
##

exefile=dmake
if test "x$BUILD_OS" = "xwin32"; then
    exefile=dmake.exe
fi

##
## Add the output bin directory to the path, as the tic program is used during
## the build process.
##

export PATH="$PATH${PATH_SEPARATOR-:}$OUTPUT_DIRECTORY/bin"

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure-tools)
    export AR='ar'
    sh ${SOURCE_DIRECTORY}/configure --prefix="$OUTPUT_DIRECTORY" \
                                     CFLAGS="$CFLAGS" \

    ;;

  configure)
    export CC="$TARGET-gcc"
    export _os="Minoca"
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --prefix=/usr \
                                     CFLAGS="$CFLAGS"

    ;;

  build-tools)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install
    ;;

  build)
    $MAKE $PARALLEL_MAKE $EXTRA_MAKEFLAGS
    $MAKE install DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

