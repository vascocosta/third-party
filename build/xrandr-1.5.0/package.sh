##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the xrandr application.
##
## Author:
##
##     Evan Green 4-Apr-2017
##
## Environment:
##
##     Build
##

. ../package_common.sh

##
## Avoid trailing slashes in the cp commands, as cp on Darwin copies the
## contents of the directory (rather than the directory itself) if there are
## trailing slashes on either the source or the destination.
##

rm -rf "$PACKAGE_DIRECTORY/usr"
mkdir -p "$PACKAGE_DIRECTORY/usr"
rm -f $BUILD_DIRECTORY/usr/lib/*.la
rm -rf "$BUILD_DIRECTORY/usr/share/man"
cp -Rpv "$BUILD_DIRECTORY/usr" "$PACKAGE_DIRECTORY"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: xrandr
Depends: libx11, libxrandr, libxrender, libgcc
Priority: optional
Version: 1.5.0
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: https://www.x.org/pub/individual/app/xrandr-1.5.0.tar.bz2
Installed-Size: $INSTALLED_SIZE
Description: Primitive command line interface to RandR extension.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

