##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the ninja build tool.
##
## Author:
##
##     Evan Green 20-Jul-2016
##
## Environment:
##
##     Build
##

. ../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY/usr/bin"
cp -pv "$BUILD_DIRECTORY/bin/ninja" "$PACKAGE_DIRECTORY/usr/bin"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: ninja
Priority: optional
Version: 1.7.1
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: https://github.com/ninja-build/ninja/archive/v1.7.1.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: Ninja build assembler.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

