##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages samba
##
## Author:
##
##     Evan Green 13-Apr-2017
##
## Environment:
##
##     Build
##

. ../package_common.sh

##
## Avoid trailing slashes in the cp commands, as cp on Darwin copies the
## contents of the directory (rather than the directory itself) if there are
## trailing slashes on either the source or the destination.
##

rm -rf "$PACKAGE_DIRECTORY/usr"
mkdir -p "$PACKAGE_DIRECTORY/usr"
rm -rf "$BUILD_DIRECTORY/usr/share/man"
cp -Rpv "$BUILD_DIRECTORY/usr" "$PACKAGE_DIRECTORY"
cp -Rpv "$BUILD_DIRECTORY/etc" "$PACKAGE_DIRECTORY"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: samba
Depends: libiconv, gnutls, openldap, gettext, libgcc
Priority: optional
Version: 4.6.2
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: https://download.samba.org/pub/samba/samba-4.6.2.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: Samba SMB server and client.
_EOS

cat > "$PACKAGE_DIRECTORY/CONTROL/conffiles" <<_EOS
/etc/samba/smb.conf.default
/etc/openldap/schema/get_next_oid
/etc/openldap/schema/ol-schema-migrate.pl
/etc/openldap/schema/samba-nds.schema
/etc/openldap/schema/samba-schema-FDS.ldif
/etc/openldap/schema/samba-schema.IBMSecureWay
/etc/openldap/schema/samba.ldif
/etc/openldap/schema/samba.schema
/etc/openldap/schema/samba.schema.at.IBM-DS
/etc/openldap/schema/samba.schema.oc.IBM-DS
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

