##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the yasm assembler/disassembler.
##
## Author:
##
##     Chris Stevens 2-Jan-2017
##
## Environment:
##
##     Build
##

. ../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/bin" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/include" "$PACKAGE_DIRECTORY/usr"
cp -Rp "$BUILD_DIRECTORY/usr/lib" "$PACKAGE_DIRECTORY/usr"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: yasm
Priority: optional
Version: 1.3.0
Architecture: $PACKAGE_ARCH
Maintainer: Chris Stevens <chris@minocacorp.com>
Section: main
Source: http://www.tortall.net/projects/yasm/releases/yasm-1.3.0.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: Yasm x86 assembler and disassembler.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

