################################################################################
#
#   Copyright (c) 2017 Minoca Corp. All Rights Reserved.
#
#   Module Name:
#
#       gcc-6.3.0
#
#   Abstract:
#
#       This makefile is responsible for building the GCC compiler toolchain.
#
#   Author:
#
#       Evan Green 18-Jan-2017
#
#   Environment:
#
#       Build
#
################################################################################

include ../common.mk

PACKAGE := gcc-6.3.0

PATCHED_SOURCE_DIRECTORY := $(OBJROOT)/$(PACKAGE).src
ORIGINAL_SOURCE_DIRECTORY := $(OBJROOT)/$(PACKAGE).orig
SOURCE_TARBALL := $(ROOT)/third-party/src/$(PACKAGE).tar.gz

TOOLBUILDROOT := $(OBJROOT)/$(PACKAGE).tool
BUILDROOT := $(OBJROOT)/$(PACKAGE).build
BINROOT := $(BUILDROOT)/build.out
PKGROOT := $(BUILDROOT)/build.pkg

DIFF_FILE := $(CURDIR)/$(PACKAGE).diff

BOOTSTRAP_COMPILER := CC=$(TOOLBUILDROOT)/gcc/xgcc AR=$(TOOLBINROOT)/bin/$(TARGET)-ar CECHO_CYAN=echo
TOOL_BUILD_TOOLDIR := $(TOOLBINROOT)/$(TARGET)
BOOTSTRAP_CPPFLAGS := $(CFLAGS) -B$(TOOLBUILDROOT)/gcc \
                      -B$(TOOL_BUILD_TOOLDIR)/bin \
                      -B$(TOOL_BUILD_TOOLDIR)/lib \
                      -I$(TOOLBINROOT)/usr/include

BOOTSTRAP_LDFLAGS := -B$(TOOLBUILDROOT)/gcc -B$(TOOL_BUILD_TOOLDIR)/bin -B$(TOOL_BUILD_TOOLDIR)/lib

.PHONY: all clean recreate-diff tools install-wintools package
.PHONY: tool-gcc-phase-1 tool-build-libc tool-copy-libc tool-gcc-phase-2 tool-copy-headers
.PHONY: build-libc copy-libc

all: copy-libc $(BUILDROOT)/Makefile
	@echo Building $(PACKAGE)
	+sh ./build.sh "$(PATCHED_SOURCE_DIRECTORY)" "$(BUILDROOT)" "$(BINROOT)" build
	$(MAKE) package
	@echo Finished building $(PACKAGE)

clean:
	rm -rf $(BUILDROOT)
	rm -rf $(TOOLBUILDROOT)
	rm -rf $(PATCHED_SOURCE_DIRECTORY)
	rm -rf $(ORIGINAL_SOURCE_DIRECTORY)

package:
	sh ./package.sh "$(BINROOT)" "$(PKGROOT)"

##
## This target recreates the diff file from the patched source directory.
##

recreate-diff: $(ORIGINAL_SOURCE_DIRECTORY)
	cd $(PATCHED_SOURCE_DIRECTORY) && diff -Nru3 -x .svn ../$(PACKAGE).orig . > $(DIFF_FILE) || test $$? = "1"

##
## Unpack the source to PACKAGE.orig.
##

$(ORIGINAL_SOURCE_DIRECTORY): $(SOURCE_TARBALL) | $(OBJROOT)
	rm -rf $(ORIGINAL_SOURCE_DIRECTORY)
	tar -xzf $^ -C $(OBJROOT)
	mv $(OBJROOT)/$(PACKAGE) $@

tools: tool-gcc-phase-2

##
## Phase 2 builds the rest of GCC now that the C library is in its final
## location.
##

tool-gcc-phase-2: tool-copy-libc
	@echo Building $(PACKAGE) Phase 2
	+sh ./build.sh "$(PATCHED_SOURCE_DIRECTORY)" "$(TOOLBUILDROOT)" "$(TOOLBINROOT)" build-tools

tool-copy-libc: tool-build-libc
	@echo Copying C Library
	sh ./copylibc.sh "$(ROOT)" "$(TOOLBINROOT)" yes

copy-libc: build-libc
	@echo Copying C Library
	sh ./copyheaders.sh "$(ROOT)" "$(BINROOT)/usr"
	sh ./copylibc.sh "$(ROOT)" "$(BINROOT)/usr"

##
## Build just the directories of the OS needed to get the C library. None of
## these should be dependent on libgcc (which has yet to be built). kernel/ke
## is in the mix because it generates kernel-version, which is needed by
## other third party packages like opkg.
##

tool-build-libc: tool-gcc-phase-1
	@echo Building C Library
	$(MAKE) -C $(ROOT)/os/lib $(BOOTSTRAP_COMPILER) CPPFLAGS="$(BOOTSTRAP_CPPFLAGS)" LDFLAGS="$(BOOTSTRAP_LDFLAGS)"
	$(MAKE) -C $(ROOT)/os/apps/osbase $(BOOTSTRAP_COMPILER) CPPFLAGS="$(BOOTSTRAP_CPPFLAGS)" LDFLAGS="$(BOOTSTRAP_LDFLAGS)"
	$(MAKE) -C $(ROOT)/os/apps/libc $(BOOTSTRAP_COMPILER) CPPFLAGS="$(BOOTSTRAP_CPPFLAGS)" LDFLAGS="$(BOOTSTRAP_LDFLAGS)"
	$(MAKE) -C $(ROOT)/os/kernel/ke $(BOOTSTRAP_COMPILER) CPPFLAGS="$(BOOTSTRAP_CPPFLAGS)" LDFLAGS="$(BOOTSTRAP_LDFLAGS)"

build-libc:
	@echo Building C Library
	$(MAKE) -C $(ROOT)/os/lib CC="$(TARGET)-gcc" CECHO_CYAN=echo
	$(MAKE) -C $(ROOT)/os/apps/osbase CC="$(TARGET)-gcc" CECHO_CYAN=echo
	$(MAKE) -C $(ROOT)/os/apps/libc CC="$(TARGET)-gcc" CECHO_CYAN=echo
	$(MAKE) -C $(ROOT)/os/kernel/ke CC="$(TARGET)-gcc" CECHO_CYAN=echo

##
## Build gcc up to the "all-gcc" target. This stops just shy of libgcc, which
## requires a C library and some headers to be in place.
##

tool-gcc-phase-1: tool-copy-headers
	@echo Building $(PACKAGE) Phase 1
	+sh ./build.sh "$(PATCHED_SOURCE_DIRECTORY)" "$(TOOLBUILDROOT)" "$(TOOLBINROOT)" build-all-gcc

tool-copy-headers: $(TOOLBUILDROOT)/Makefile
	@echo Copying C Library headers
	sh ./copyheaders.sh "$(ROOT)" "$(TOOLBINROOT)/usr"

$(TOOLBUILDROOT)/Makefile: | $(TOOLBINROOT) $(TOOLBUILDROOT) $(PATCHED_SOURCE_DIRECTORY)
	@echo Configuring $(PACKAGE)
	+sh ./build.sh "$(PATCHED_SOURCE_DIRECTORY)" "$(TOOLBUILDROOT)" "$(TOOLBINROOT)" configure-tools

$(BUILDROOT)/Makefile: | $(BINROOT) $(BUILDROOT) $(PATCHED_SOURCE_DIRECTORY)
	@echo Configuring $(PACKAGE)
	+sh ./build.sh "$(PATCHED_SOURCE_DIRECTORY)" "$(BUILDROOT)" "$(BINROOT)" configure

##
## Unpack the source to PACKAGE.src, then apply the patch.
##

$(PATCHED_SOURCE_DIRECTORY): $(SOURCE_TARBALL)
	rm -rf $(PATCHED_SOURCE_DIRECTORY)
	tar -xzf $(SOURCE_TARBALL) -C $(OBJROOT)
	mv $(OBJROOT)/$(PACKAGE) $@
	cd $(PATCHED_SOURCE_DIRECTORY) && $(PATCH) -p0 -i $(DIFF_FILE)

$(TOOLBUILDROOT) $(BUILDROOT) $(BINROOT):
	mkdir -p $@

