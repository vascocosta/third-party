##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the minidlna application.
##
## Author:
##
##     Chris Stevens 14-Mar-2017
##
## Environment:
##
##     Build
##

. ../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY/usr"
mkdir -p "$PACKAGE_DIRECTORY/etc"
cp -Rpv "$BUILD_DIRECTORY/usr/sbin" "$PACKAGE_DIRECTORY/usr"
cp -pv ./minidlna.conf "$PACKAGE_DIRECTORY/etc"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: minidlna
Depends: libid3tag, sqlite, ffmpeg, libvorbis, libogg, libexif, flac
Priority: optional
Version: 1.1.5
Architecture: $PACKAGE_ARCH
Maintainer: Chris Stevens <chris@minocacorp.com>
Section: main
Source: http://downloads.sourceforge.net/project/minidlna/minidlna/1.1.5/minidlna-1.1.5.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: MiniDLNA media file server.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

