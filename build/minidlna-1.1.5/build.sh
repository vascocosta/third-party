##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the minidlna package.
##
## Author:
##
##     Chris Stevens 14-Mar-2017
##
## Environment:
##
##     Build
##

. ../build_common.sh

FFMPEG=ffmpeg_3.2.1
LIBJPEG=libjpeg-turbo_1.5.1
SQLITE=sqlite_3080500
LIBEXIF=libexif_0.6.21
LIBID3TAG=libid3tag_0.15.1b
LIBOGG=libogg_1.3.2
LIBVORBIS=libvorbis_1.3.5
FLAC=flac_1.3.2

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    extract_dependency "$FFMPEG"
    extract_dependency "$LIBJPEG"
    extract_dependency "$SQLITE"
    extract_dependency "$LIBEXIF"
    extract_dependency "$LIBID3TAG"
    extract_dependency "$LIBOGG"
    extract_dependency "$LIBVORBIS"
    extract_dependency "$FLAC"

    export CFLAGS="$CFLAGS -I$DEPENDROOT/usr/include"
    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    export CC="$TARGET-gcc"
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --disable-nls \
                                     --prefix="/usr" \
                                     --sysconfdir="/etc" \
                                     --localstatedir="/var" \
                                     --enable-static=no \
                                     --program-prefix="" \
                                     --program-suffix="" \
                                     CFLAGS="$CFLAGS" \
                                     LDFLAGS="$LDFLAGS"

    ;;

  build)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install-strip DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

