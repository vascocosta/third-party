#
# This is the configuration file for the MiniDLNA daemon, a DLNA/UPnP-AV media
# server, on Minoca OS.
#
# Unless otherwise noted, the commented out options show their default values.
#

# Port for HTTP (descriptions, SOAP, media transfer) traffic.
port=8200

# Specify the user account name or uid to run as.
#user=minidlna

# Set this to the directory you want scanned.
# * If you want multiple directories, you can have multiple media_dir= lines
# * If you want to restrict a media_dir to specific content types, you
#   can prepend the types, followed by a comma, to the directory:
#   + "A" for audio  (eg. media_dir=A,/var/lib/minidlna/Music)
#   + "V" for video  (eg. media_dir=V,/var/lib/minidlna/Videos)
#   + "P" for images (eg. media_dir=P,/var/lib/minidlna/Pictures)
#   + "PV" for pictures and video (eg. media_dir=PV,/var/lib/minidlna/digital_camera)
media_dir=/var/lib/minidlna

# Set this to merge all media_dir base contents into the root container.
# Note: the default is no
#merge_media_dirs=no

# Set this if you want to customize the name that shows up on your clients.
#friendly_name=My DLNA Server

# Set this if you would like to specify the directory where you want MiniDLNA
# to store its database and album art cache.
#db_dir=/var/cache/minidlna

# Set this if you would like to specify the directory where you want MiniDLNA
# to store its log file.
#log_dir=/var/log

# Set this to change the verbosity of the information that is logged. Each
# section can use a different level: off, fatal, error, warn, info, or debug.
#log_level=general,artwork,database,inotify,scanner,metadata,http,ssdp,tivo=warn

# This should be a list of file names to check for when searching for album art.
# Note: names should be delimited with a forward slash ("/").
album_art_names=Cover.jpg/cover.jpg/AlbumArtSmall.jpg/albumartsmall.jpg/AlbumArt.jpg/albumart.jpg/Album.jpg/album.jpg/Folder.jpg/folder.jpg/Thumb.jpg/thumb.jpg

# Set this to no to disable inotify monitoring to automatically discover new
# files.
# Note: the default is yes.
# Note: Minoca OS does not support inotify.
inotify=yes

# Set this to yes to enable support for streaming .jpg and .mp3 files to a TiVo
# supporting HMO.
enable_tivo=no

# Set this to strictly adhere to DLNA standards.
# * This will allow server-side downscaling of very large JPEG images,
#   which may hurt JPEG serving performance on (at least) Sony DLNA products.
strict_dlna=no

# Default presentation url is http address on port 80.
#presentation_url=http://www.mylan/index.php

# Notify interval in seconds. The default is 895 seconds.
notify_interval=900

# Serial and model number the daemon will report to clients in its XML
# description.
serial=12345678
model_number=1

# Specify the path to the MiniSSDPd socket.
#minissdpdsocket=/var/run/minissdpd.sock

# Use different container as root of the tree.
# Possible values:
#   + "." - use standard container (this is the default)
#   + "B" - "Browse Directory"
#   + "M" - "Music"
#   + "V" - "Video"
#   + "P" - "Pictures"
#   + Or, you can specify the ObjectID of your desired root container
#     (eg. 1$F for Music/Playlists)
# If you specify "B" and client device is audio-only then "Music/Folders" will
# be used as root
#root_container=.

# Always force SortCriteria to this value, regardless of the SortCriteria
# passed by the client.
#force_sort_criteria=+upnp:class,+upnp:originalTrackNumber,+dc:title

# Maximum number of simultaneous connections.
# Note: many clients open several simultaneous connections while streaming.
#max_connections=50
