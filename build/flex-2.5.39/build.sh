##
## Copyright (c) 2014 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the flex package.
##
## Author:
##
##     Evan Green 1-May-2014
##
## Environment:
##
##     Build
##

. ../build_common.sh

##
## Flex squirrels away the complete path to M4, which then doesn't work if m4
## is moved. Set this directly to avoid hardcoded absolute paths.
##

export ac_cv_path_M4='m4'

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure-tools)
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --prefix="/" \
                                     --disable-shared

    ;;

  configure)

    ##
    ## Export some variables needed to cross compile correctly.
    ##

    if test "$BUILD_OS" != "minoca"; then
        export ac_cv_func_malloc_0_nonnull=yes
        export ac_cv_func_realloc_0_nonnull=yes
    fi

    export CC="$TARGET-gcc"
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --prefix="/usr" \
                                     CFLAGS="$CFLAGS"

    ;;

  build-tools)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install-strip prefix="$OUTPUT_DIRECTORY"
    ;;

  build)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install-strip prefix="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

