################################################################################
#
#   Copyright (c) 2016 Minoca Corp. All Rights Reserved.
#
#   Module Name:
#
#       setuptools-23.0.0
#
#   Abstract:
#
#       This makefile is responsible for building the Python setuptools package.
#
#   Author:
#
#       Chris Stevens 15-Jun-2016
#
#   Environment:
#
#       Build
#
################################################################################

include ../common.mk

PACKAGE := setuptools-23.0.0
PATCHED_SOURCE_DIRECTORY := $(OBJROOT)/$(PACKAGE).src
SOURCE_TARBALL := $(ROOT)/third-party/src/$(PACKAGE).tar.gz

BUILDROOT := $(OBJROOT)/$(PACKAGE).build
BINROOT := $(BUILDROOT)/build.out
PKGROOT := $(BUILDROOT)/build.pkg

.PHONY: all clean package

all: | $(BINROOT) $(BUILDROOT) $(PATCHED_SOURCE_DIRECTORY)
	@echo Building $(PACKAGE)
	+sh ./build.sh "$(PATCHED_SOURCE_DIRECTORY)" "$(BUILDROOT)" "$(BINROOT)" build
	$(MAKE) package

clean:
	rm -rf $(BUILDROOT)
	rm -rf $(PATCHED_SOURCE_DIRECTORY)

package:
	sh ./package.sh "$(BINROOT)" "$(PKGROOT)"

##
## Unpack the source to PACKAGE.src, then apply the patch.
##

$(PATCHED_SOURCE_DIRECTORY): $(SOURCE_TARBALL) | $(OBJROOT)
	rm -rf $(PATCHED_SOURCE_DIRECTORY)
	tar -xzf $(SOURCE_TARBALL) -C $(OBJROOT)
	mv $(OBJROOT)/$(PACKAGE) $@

$(BUILDROOT) $(BINROOT):
	mkdir -p $@

