##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the Boehm garbage collection library.
##
## Author:
##
##     Evan Green 28-Jul-2016
##
## Environment:
##
##     Build
##

. ../package_common.sh

rm -rf "$PACKAGE_DIRECTORY/usr"
mkdir -p "$PACKAGE_DIRECTORY/usr"

##
## Remove the libtool garbage.
##

rm "$BUILD_DIRECTORY"/usr/lib/*.la
cp -Rpv "$BUILD_DIRECTORY/usr/include" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/lib" "$PACKAGE_DIRECTORY/usr"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: libgc
Depends: libgcc
Priority: optional
Version: 7.4.4
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: http://www.hboehm.info/gc/gc_source/gc-7.4.4.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: Boehm garbage collection library.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

