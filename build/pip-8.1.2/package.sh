##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages Python pip.
##
## Author:
##
##     Chris Stevens 16-Jun-2016
##
## Environment:
##
##     Build
##

. ../package_common.sh

cp -Rpv "$BUILD_DIRECTORY/usr" "$PACKAGE_DIRECTORY"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: python-pip
Depends: python-setuptools
Priority: optional
Version: 8.1.2
Architecture: $PACKAGE_ARCH
Maintainer: Chris Stevens <chris@minocacorp.com>
Section: main
Source: https://pypi.python.org/packages/e7/a8/7556133689add8d1a54c0b14aeff0acb03c64707ce100ecd53934da1aa13/pip-8.1.2.tar.gz#md5=87083c0b9867963b29f7aba3613e8f4a
Installed-Size: $INSTALLED_SIZE
Description: Pip Installs Packages is a tool to easily install Python packages.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

