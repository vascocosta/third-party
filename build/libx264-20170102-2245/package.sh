##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the x264 video encoding library.
##
## Author:
##
##     Chris Stevens 18-Jan-2017
##
## Environment:
##
##     Build
##

. ../package_common.sh

cp -Rpv "$BUILD_DIRECTORY/usr" "$PACKAGE_DIRECTORY"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: libx264
Priority: optional
Version: 20170102-2245
Architecture: $PACKAGE_ARCH
Maintainer: Chris Stevens <chris@minocacorp.com>
Section: main
Source: ftp://ftp.videolan.org/pub/videolan/x264/snapshots/x264-snapshot-20170102-2245-stable.tar.bz2
Installed-Size: $INSTALLED_SIZE
Description: x264 library for encoding video streams into the H.264/MPEG-4 AVC
 compression format.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

