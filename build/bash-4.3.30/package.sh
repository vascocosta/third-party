##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the bash shell.
##
## Author:
##
##     Evan Green 31-Mar-2015
##
## Environment:
##
##     Build
##

. ../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY/usr/bin"
cp -pv "$BUILD_DIRECTORY/usr/bin/bash" "$PACKAGE_DIRECTORY/usr/bin"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: bash
Priority: optional
Version: 4.3.30
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: https://ftp.gnu.org/gnu/bash/bash-4.3.30.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: GNU Bourne Again shell.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

