##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the Ruby core.
##
## Author:
##
##     Chris Stevens 29-Jun-2016
##
## Environment:
##
##     Build
##

. ../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/bin" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/include" "$PACKAGE_DIRECTORY/usr"
cp -Rp "$BUILD_DIRECTORY/usr/lib" "$PACKAGE_DIRECTORY/usr"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: ruby
Priority: optional
Version: 2.3.1
Architecture: $PACKAGE_ARCH
Maintainer: Chris Stevens <chris@minocacorp.com>
Section: main
Source: https://cache.ruby-lang.org/pub/ruby/2.3/ruby-2.3.1.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: Ruby is an interpreted scripting language.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

