##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the Ruby package.
##
## Author:
##
##     Chris Stevens 29-Jun-2016
##
## Environment:
##
##     Build
##

. ../build_common.sh

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    if test "x$BUILD_OS" != "xminoca"; then
        echo "Error: Ruby cannot be cross-compiled."
        exit 3
    fi

    ##
    ## Both configure and configure.in are patched, but if configure.in
    ## happens to be patched later then autoconf will kick in to try an rebuild
    ## configure. Explicitly touch configure after patching to avoid that
    ## happening.
    ##

    touch ${SOURCE_DIRECTORY}/configure
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE         \
                                     --host="$TARGET"    \
                                     --enable-shared     \
                                     --prefix="/usr"     \
                                     --sysconfdir="/etc" \
                                     CC="$TARGET-gcc"    \
                                     CXX="$TARGET-g++"   \
                                     CFLAGS="$CFLAGS"    \
                                     CXXFLAGS="$CFLAGS"  \
                                     LDFLAGS="$LDFLAGS"

    ;;

  build)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

