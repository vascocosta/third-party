##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the openLDAP suite.
##
## Author:
##
##     Evan Green 2-Sep-2016
##
## Environment:
##
##     Build
##

. ../package_common.sh

rm -f "$BUILD_DIRECTORY"/usr/lib/*.la
mkdir -p "$PACKAGE_DIRECTORY/usr"
cp -Rpfv "$BUILD_DIRECTORY/etc" "$PACKAGE_DIRECTORY"
cp -Rpfv "$BUILD_DIRECTORY/var" "$PACKAGE_DIRECTORY"
cp -Rpfv "$BUILD_DIRECTORY/usr/bin" "$PACKAGE_DIRECTORY/usr"
cp -Rpfv "$BUILD_DIRECTORY/usr/include" "$PACKAGE_DIRECTORY/usr"
cp -Rpfv "$BUILD_DIRECTORY/usr/lib" "$PACKAGE_DIRECTORY/usr"
cp -Rpfv "$BUILD_DIRECTORY/usr/sbin" "$PACKAGE_DIRECTORY/usr"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: openldap
Depends: libopenssl, libtool, libgcc
Priority: optional
Version: 2.4.44
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: ftp://ftp.openldap.org/pub/OpenLDAP/openldap-release/openldap-2.4.44.tgz
Installed-Size: $INSTALLED_SIZE
Description: Open LDAP server and client.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

