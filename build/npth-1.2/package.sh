##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the gnupg npth library.
##
## Author:
##
##     Evan Green 31-Aug-2016
##
## Environment:
##
##     Build
##

. ../package_common.sh

rm "$BUILD_DIRECTORY"/usr/lib/*.la
rm -rf "$BUILD_DIRECTORY"/usr/share/info

mkdir -p "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/bin" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/include" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/lib" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/share" "$PACKAGE_DIRECTORY/usr"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: libnpth
Depends: libgcc
Priority: optional
Version: 1.2
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: https://gnupg.org/ftp/gcrypt/npth/npth-1.2.tar.bz2
Installed-Size: $INSTALLED_SIZE
Description: GNUPG new threading library.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

