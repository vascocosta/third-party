##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the readline utility.
##
## Author:
##
##     Evan Green 16-Feb-2015
##
## Environment:
##
##     Build
##

. ../package_common.sh

rm -rf "$PACKAGE_DIRECTORY/usr"
mkdir -p "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/include" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/lib" "$PACKAGE_DIRECTORY/usr"
mkdir -p "$PACKAGE_DIRECTORY/usr/share"
cp -Rpv "$BUILD_DIRECTORY/usr/share/readline" "$PACKAGE_DIRECTORY/usr/share"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: libreadline
Depends: libncurses
Priority: optional
Version: 6.3
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: http://ftp.gnu.org/gnu/readline/readline-6.3.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: The GNU readline library assists programs in line-editing
 functions.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

