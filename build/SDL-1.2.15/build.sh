##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the SDL library.
##
## Author:
##
##     Evan Green 21-Feb-2017
##
## Environment:
##
##     Build
##

. ../build_common.sh

LIBGCC=libgcc_6.3.0

SOURCE_DIRECTORY=`echo $SOURCE_DIRECTORY | sed 's/[a-zA-Z]://'`
cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    extract_dependency "$LIBGCC" || echo "Ignoring libgcc."
    extract_dependency libx11_1.6.5
    extract_dependency libxinerama_1.1.3
    extract_dependency libxi_1.7.9
    extract_dependency libxrender_0.9.10
    extract_dependency libxrandr_1.5.1
    extract_dependency libxscrnsaver_1.2.2
    extract_dependency libxcursor_1.1.14
    extract_dependency libxxf86vm_1.1.4
    extract_dependency mesa_17.0.0
    extract_dependency libice_1.0.9

    extract_dependency libpixman_0.34.0
    extract_dependency libxcb_1.12
    extract_dependency libxau_1.0.8
    extract_dependency xproto_7.0.31
    extract_dependency renderproto_0.11.1
    extract_dependency kbproto_1.0.7
    extract_dependency libxext_1.3.3
    extract_dependency xextproto_7.3.0
    extract_dependency libiconv_1.14
    extract_dependency libffi_3.2.1
    extract_dependency libxml2_2.9.4
    extract_dependency xineramaproto_1.2.1
    extract_dependency fontconfig_2.12.1
    extract_dependency inputproto_2.3.2
    extract_dependency scrnsaverproto_1.2.2
    extract_dependency xf86vidmodeproto_2.3.1
    extract_dependency libxfixes_5.0.3
    extract_dependency fixesproto_5.0
    extract_dependency randrproto_1.5.0

    export CC="$TARGET-gcc"
    export CPPFLAGS="$CPPFLAGS -I$DEPENDROOT/usr/include/directfb \
 -I$DEPENDROOT/usr/include"

    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --disable-static \
                                     --enable-assertions=enabled \
                                     --with-x \
                                     --disable-rpath \
                                     --prefix="/usr" \
                                     --srcdir="$SOURCE_DIRECTORY" \
                                     CPPFLAGS="$CPPFLAGS" \
                                     CFLAGS="$CFLAGS" \
                                     LDFLAGS="$LDFLAGS"

    ;;

  build)
    $MAKE $PARALLEL_MAKE V=1
    $MAKE $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

