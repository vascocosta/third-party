##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the screen utility.
##
## Author:
##
##     Evan Green 29-Jul-2016
##
## Environment:
##
##     Build
##

. ../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY/usr/bin"
cp -pv "$BUILD_DIRECTORY/usr/bin/screen-4.4.0" "$PACKAGE_DIRECTORY/usr/bin/screen"
chmod 755 "$PACKAGE_DIRECTORY/usr/bin/screen"
mkdir -p "$PACKAGE_DIRECTORY/usr/share"
cp -Rpv "$BUILD_DIRECTORY/usr/share/screen" "$PACKAGE_DIRECTORY/usr/share"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: screen
Depends: libncurses, libgcc
Priority: optional
Version: 4.4.0
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: https://ftp.gnu.org/gnu/screen/screen-4.4.0.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: GNU screen utility.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

