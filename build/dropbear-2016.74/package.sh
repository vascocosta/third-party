##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the dropbear utility.
##
## Author:
##
##     Evan Green 3-Aug-2016
##
## Environment:
##
##     Build
##

. ../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY"
cp -Rpv "$BUILD_DIRECTORY/usr" "$PACKAGE_DIRECTORY"

##
## Create an init script.
##

mkdir -p "$PACKAGE_DIRECTORY/etc/init.d"
cat > "$PACKAGE_DIRECTORY/etc/init.d/dropbear" <<_EOS
#! /bin/sh
set -e

# /etc/init.d/dropbear: start and stop the Dropbear SSH 2 server.

DROPBEAR=/usr/sbin/dropbear
DROPBEARKEY=/usr/bin/dropbearkey
KEYDIR=/etc/dropbear
RSAKEY=\$KEYDIR/dropbear_rsa_host_key
DSSKEY=\$KEYDIR/dropbear_dss_host_key
PIDFILE=/var/run/dropbear.pid
test -x \$DROPBEAR || exit 0

[ -f /etc/init.d/init-functions ] && . /etc/init.d/init-functions

umask 022
if test -f /etc/default/dropbear; then
    . /etc/default/dropbear
fi

if [ -n \$2 ]; then
    DROPBEAR_OPTS="\$DROPBEAR_OPTS \$2"
fi

check_keys() {
    # create keys if necessary
    [ -d "\$KEYDIR" ] || mkdir -p "\$KEYDIR"
    if [ ! -f \$RSAKEY ]; then
        log_daemon_msg "  generating RSA key..."
        \$DROPBEARKEY -t rsa -s 2048 -f \$RSAKEY
    fi
    if [ ! -f \$DSSKEY ]; then
        log_daemon_msg "  generating DSS key..."
        \$DROPBEARKEY -t dss -f \$DSSKEY
    fi
}

export PATH="\${PATH:+\$PATH:}/usr/sbin:/sbin"

case "\$1" in
    start)
        log_daemon_msg "Starting Dropbear SSH server" || true
        [ -d /var/run ] || mkdir -p /var/run
        check_keys
        if \$DROPBEAR -d \$DSSKEY -r \$RSAKEY -P \$PIDFILE \$DROPBEAR_OPTS ;
        then

            log_end_msg 0 || true

        else
            log_end_msg 1 || true

        fi
        ;;

    stop)
        log_daemon_msg "Stopping Dropbear SSH server" || true
        if start-stop-daemon -K -p \$PIDFILE -qo -n dropbear; then
            log_end_msg 0 || true

        else
            log_end_msg 1 || true
        fi
        ;;

    restart)
        \$0 stop
        \$0 start
        ;;

    *)
        log_action_msg "Usage: /etc/init.d/dropbear {start|stop|restart}"
        exit 1
        ;;

esac

exit 0
_EOS
chmod +x "$PACKAGE_DIRECTORY/etc/init.d/dropbear"

##
## Create a pre-remove script.
##

cat > "$PACKAGE_DIRECTORY/CONTROL/prerm" <<"_EOS"
#! /bin/sh
if test "x$D" != "x"; then
    exit 1
else
    /etc/init.d/dropbear stop
    update-rc.d -f dropbear remove
    rm -f /etc/init.d/dropbear
fi
_EOS

chmod +x "$PACKAGE_DIRECTORY/CONTROL/prerm"

##
## Create a post-install script.
##

cat > "$PACKAGE_DIRECTORY/CONTROL/postinst" <<"_EOS"
#! /bin/sh

set -x
ROOTLINE=
CHROOTCMD=
if test "$PKG_ROOT" != "/"; then
    ROOTLINE="-R$PKG_ROOT"
    CHROOTCMD="chroot $PKG_ROOT -- "
fi

if test "x$D" != "x"; then
    exit 1

else
    $CHROOTCMD /usr/sbin/update-rc.d dropbear defaults
fi
_EOS

chmod +x "$PACKAGE_DIRECTORY/CONTROL/postinst"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: dropbear
Priority: optional
Version: 2016.74
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: https://matt.ucc.asn.au/dropbear/releases/dropbear-2016.74.tar.bz2
Installed-Size: $INSTALLED_SIZE
Description: Dropbear SSH client and server.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

