##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the PNG library.
##
## Author:
##
##     Evan Green 7-Mar-2017
##
## Environment:
##
##     Build
##

. ../build_common.sh

ZLIB=libz_1.2.11

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    export CC="$TARGET-gcc"
    extract_dependency "$ZLIB"
    export CPPFLAGS="$CPPFLAGS -I$DEPENDROOT/usr/include \
 -I$DEPENDROOT/usr/include/glib-2.0 -I$DEPENDROOT/usr/lib/glib-2.0/include"

    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --prefix="/usr" \
                                     --sysconfdir=/etc \
                                     --disable-static \
                                     CPPFLAGS="$CPPFLAGS" \
                                     CFLAGS="$CFLAGS" \
                                     LDFLAGS="$LDFLAGS"

    ;;

  build)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

