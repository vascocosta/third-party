##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the shadow utility.
##
## Author:
##
##     Evan Green 3-Mar-2015
##
## Environment:
##
##     Build
##

. ../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/bin" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/sbin" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/etc" "$PACKAGE_DIRECTORY"

##
## Move passwd out from usr and into bin directly.
##

mkdir -p "$PACKAGE_DIRECTORY/bin"
mv "$PACKAGE_DIRECTORY/usr/bin/passwd" "$PACKAGE_DIRECTORY/bin"

##
## Change some login defaults.
##

ROOT_PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
NORMAL_PATH=/usr/local/bin:/usr/bin:/bin

sed -e 's/^CONSOLE/#CONSOLE/' \
    -e 's|/var/spool/mail|/var/mail|' \
    -e 's/^ENV_HZ/#ENV_HZ/' \
    -e 's/^#ENCRYPT_METHOD.*/ENCRYPT_METHOD SHA512/' \
    -e "s|^ENV_SUPATH.*|ENV_SUPATH PATH=$ROOT_PATH|" \
    -e "s|^ENV_PATH.*|ENV_PATH PATH=$NORMAL_PATH|" \
    $BUILD_DIRECTORY/etc/login.defs > $PACKAGE_DIRECTORY/etc/login.defs

trim_cr $PACKAGE_DIRECTORY/etc/login.defs

sed -e 's|SHELL=/bin/bash|SHELL=/bin/sh|' \
    -e 's|CREATE_MAIL_SPOOL=yes|CREATE_MAIL_SPOOL=no|' \
    $BUILD_DIRECTORY/etc/default/useradd > \
    $PACKAGE_DIRECTORY/etc/default/useradd

trim_cr $PACKAGE_DIRECTORY/etc/default/useradd

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: shadow
Priority: optional
Version: 4.2.1
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: http://pkg-shadow.alioth.debian.org/releases/shadow-4.2.1.tar.xz
Installed-Size: $INSTALLED_SIZE
Description: Shadowed password file utilities like useradd, su, and passwd.
_EOS

cat > "$PACKAGE_DIRECTORY/CONTROL/conffiles" <<_EOS
/etc/limits
/etc/login.access
/etc/login.defs
/etc/default/useradd
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

