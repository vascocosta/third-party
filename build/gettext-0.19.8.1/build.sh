##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the gettext package.
##
## Author:
##
##     Evan Green 29-Aug-2016
##
## Environment:
##
##     Build
##

. ../build_common.sh

LIBICONV=libiconv_1.14
LIBXML2=libxml2_2.9.4
LIBUNISTRING=libunistring_0.9.6
LIBNCURSES=libncurses_5.9

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    extract_dependency "$LIBICONV"
    extract_dependency "$LIBXML2"
    extract_dependency "$LIBUNISTRING"
    extract_dependency "$LIBNCURSES"
    export CPPFLAGS="$CPPFLAGS -I$DEPENDROOT/usr/include \
 -I$DEPENDROOT/usr/include/libxml2"

    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    export CC="$TARGET-gcc"
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --disable-java \
                                     --disable-native-java \
                                     --enable-threads=posix \
                                     --disable-static \
                                     --disable-rpath \
                                     --enable-relocatable \
                                     --with-libpth-prefix=$DEPENDROOT \
                                     --with-libiconv-prefix=$DEPENDROOT \
                                     --with-libintl-prefix=$DEPENDROOT \
                                     --with-libglib-2.0-prefix=$DEPENDROOT \
                                     --with-libcroco-0.6-prefix=$DEPENDROOT \
                                     --with-libunistring-prefix=$DEPENDROOT \
                                     --with-libxml2-prefix=$DEPENDROOT \
                                     --with-libncurses-prefix=$DEPENDROOT \
                                     --without-git \
                                     --without-cvs \
                                     --prefix="/usr" \
                                     CFLAGS="$CFLAGS" \
                                     CPPFLAGS="$CPPFLAGS" \
                                     LDFLAGS="$LDFLAGS"

    ;;

  build)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install-strip DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

