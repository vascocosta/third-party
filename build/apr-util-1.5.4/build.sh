##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the Apache Portable Runtime utilities
##     package.
##
## Author:
##
##     Evan Green 29-Mar-2015
##
## Environment:
##
##     Build
##

. ../build_common.sh

SQLITE=sqlite_3080500
OPENSSL=libopenssl_1.0.2h
APR=libapr_1.5.1
LIBICONV=libiconv_1.14
EXPAT=expat_2.1.0

APRLIBDIR="$DEPENDROOT/usr/lib/apr-1"
cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    export CC="$TARGET-gcc"
    extract_dependency "$SQLITE"
    OPENSSL_LINE="--with-openssl=$DEPENDROOT/usr/"
    extract_dependency "$OPENSSL" || OPENSSL_LINE=
    extract_dependency "$APR"
    extract_dependency "$LIBICONV"
    extract_dependency "$EXPAT"

    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    ##
    ## Fix up apr-1-config.
    ##

    sed -e "s|^prefix=.*|prefix=\"$DEPENDROOT/usr\"|" \
        -e "s|^installbuilddir=.*|installbuilddir=\"$APRLIBDIR\"|" \
        "$DEPENDROOT/usr/bin/apr-1-config" > \
        $DEPENDROOT/usr/bin/apr-1-config.tmp

    mv "$DEPENDROOT/usr/bin/apr-1-config.tmp" "$DEPENDROOT/usr/bin/apr-1-config"
    chmod +x "$DEPENDROOT/usr/bin/apr-1-config"
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --with-apr="$DEPENDROOT/usr/" \
                                     $OPENSSL_LINE \
                                     --with-sqlite3="$DEPENDROOT/usr/" \
                                     --with-expat="$DEPENDROOT/usr/" \
                                     --x-includes="$DEPENDROOT/usr/include" \
                                     --prefix="/usr" \
                                     CFLAGS="$CFLAGS" \
                                     LDFLAGS="$LDFLAGS"

    ;;

  build)
    $MAKE $PARALLEL_MAKE \
         apr_builddir="$APRLIBDIR" apr_builders="$APRLIBDIR" \
         includedir="$OUTPUT_DIRECTORY/usr/include/apr-1"

    $MAKE $PARALLEL_MAKE install \
         apr_builddir="$APRLIBDIR" apr_builders="$APRLIBDIR" \
         includedir="$OUTPUT_DIRECTORY/usr/include/apr-1" \
         prefix="$OUTPUT_DIRECTORY/usr"

    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

