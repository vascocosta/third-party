##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the less utility.
##
## Author:
##
##     Evan Green 28-Jun-2016
##
## Environment:
##
##     Build
##

. ../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/bin" "$PACKAGE_DIRECTORY/usr"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: less
Depends: libgcc, libncurses
Priority: optional
Version: 481
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: http://www.greenwoodsoftware.com/less/less-481.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: Less screen paging utility.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

