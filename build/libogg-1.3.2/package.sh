##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the Ogg encoding/decoding library.
##
## Author:
##
##     Chris Stevens 23-Jan-2017
##
## Environment:
##
##     Build
##

. ../package_common.sh

rm -f "$BUILD_DIRECTORY"/usr/lib/*.la

mkdir -p "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/include" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/lib" "$PACKAGE_DIRECTORY/usr"

mkdir -p "$PACKAGE_DIRECTORY/usr/share"
cp -Rpv "$BUILD_DIRECTORY/usr/share/aclocal" "$PACKAGE_DIRECTORY/usr/share"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: libogg
Priority: optional
Version: 1.3.2
Architecture: $PACKAGE_ARCH
Maintainer: Chris Stevens <chris@minocacorp.com>
Section: main
Source: http://downloads.xiph.org/releases/ogg/libogg-1.3.2.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: Library for Ogg stream encoding/decoding.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

