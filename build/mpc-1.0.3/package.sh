##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the MPC library.
##
## Author:
##
##     Evan Green 18-Jan-2017
##
## Environment:
##
##     Build
##

. ../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/include" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/lib" "$PACKAGE_DIRECTORY/usr"

##
## Libtool sucks.
##

rm $PACKAGE_DIRECTORY/usr/lib/*.la

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: libmpc
Depends: libgmp, libmpfr
Priority: optional
Version: 1.0.3
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: https://ftp.gnu.org/gnu/mpc/mpc-1.0.3.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: Gnu MPC is a C library for the arithmetic of complex numbers with
 arbitrarily high precision and correct rounding of the result.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

