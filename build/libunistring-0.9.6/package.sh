##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the unistring library.
##
## Author:
##
##     Evan Green 28-Jul-2016
##
## Environment:
##
##     Build
##

. ../package_common.sh

rm -rf "$PACKAGE_DIRECTORY/usr"
mkdir -p "$PACKAGE_DIRECTORY/usr"

##
## Remove the libtool garbage.
##

rm "$BUILD_DIRECTORY"/usr/lib/*.la
rm -f "$BUILD_DIRECTORY/usr/lib/charset.alias"
cp -Rpv "$BUILD_DIRECTORY/usr/include" "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr/lib" "$PACKAGE_DIRECTORY/usr"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: libunistring
Depends: libiconv, libgcc
Priority: optional
Version: 0.9.6
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: https://ftp.gnu.org/gnu/libunistring/libunistring-0.9.6.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: GNU Unicode string library.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

