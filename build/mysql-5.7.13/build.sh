##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the MySQL package.
##
## Author:
##
##     Chris Stevens 29-Jun-2016
##
## Environment:
##
##     Build
##

. ../build_common.sh

NCURSES=libncurses_5.9
CMAKE=cmake_3.5.2
BOOST=boost_1_59_0

big_files="item_geofunc_internal \
item_geofunc \
item_geofunc_relchecks_bgwrap \
item_geofunc_relchecks \
item_geofunc_buffer \
item_geofunc setops"

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)

    ##
    ## Cross-compiling MySQL is easier said than done. Don't allow it for now.
    ##

    if test "x$BUILD_OS" != "xminoca"; then
        echo "Error: MySQL does not cross-compile easily."
        exit 3
    fi

    ##
    ## MySQL expects either C++11 or deprecated pre-C99 macros.
    ##

    export CXXFLAGS="$CFLAGS -std=c++11"

    ##
    ## MySQL depends on ncurses, boost and uses cmake to configure. Use the
    ## versions that were built during the build process.
    ##

    extract_dependency "$NCURSES"
    extract_dependency "$CMAKE"
    extract_dependency "$BOOST"
    export PATH="$DEPENDROOT/usr/bin:$PATH"
    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    cmake -D CMAKE_FIND_ROOT_PATH="$DEPENDROOT" \
          -D CMAKE_C_FLAGS="$CFLAGS" \
          -D CMAKE_CXX_FLAGS="$CXXFLAGS" \
          -D CMAKE_EXE_LINKER_FLAGS="$LDFLAGS" \
          -D CMAKE_MODULE_LINKER_FLAGS="$LDFLAGS" \
          -D WITH_BOOST="$DEPENDROOT/usr/include" \
          -D CMAKE_INSTALL_PREFIX="/usr" ${SOURCE_DIRECTORY}

    ;;

  build)

    ##
    ## Most of mysql can be built in parallel just fine. A few files are
    ## humongous to compile, and require about 1GB of memory each. Compile
    ## those files individually and separately. Start with a prerequisite
    ## of those files, mysql-error.h.
    ##

    cd $BUILD_DIRECTORY/extra
    $MAKE $PARALLEL_MAKE COLOR=0

    ##
    ## Now build the tricky objects in the first directory.
    ##

    echo "Building giant objs in sql."
    objs=
    for o in $big_files; do
        objs="$objs $o.cc.o"
    done
    cd $BUILD_DIRECTORY/sql
    $MAKE COLOR=0 $objs

    ##
    ## Those objects are built another time in the tree as well.
    ##

    objs=
    for o in $big_files; do
        objs="$objs __/sql/$o.cc.o"
    done

    echo "Building giant objs in libmysqld."
    cd $BUILD_DIRECTORY/libmysqld
    $MAKE COLOR=0 $objs

    ##
    ## Now make the rest in full parallel glory.
    ##

    cd $BUILD_DIRECTORY
    $MAKE $PARALLEL_MAKE COLOR=0
    $MAKE install DESTDIR="$OUTPUT_DIRECTORY" COLOR=0
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

