##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the MPFR package.
##
## Author:
##
##     Evan Green 18-Jan-2017
##
## Environment:
##
##     Build
##

LIBGMP=libgmp_6.1.2

. ../build_common.sh

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure-tools)
    export AR='ar'
    export CPPFLAGS="-I$OUTPUT_DIRECTORY/include"
    export LDFLAGS="$LDFLAGS -L$OUTPUT_DIRECTORY/lib"
    sh ${SOURCE_DIRECTORY}/configure --prefix="$OUTPUT_DIRECTORY" \
                                     --enable-static \
                                     --disable-shared \
                                     CPPFLAGS="$CPPFLAGS" \
                                     CFLAGS="$CFLAGS" \
                                     LDFLAGS="$LDFLAGS" \

    ;;

  configure)
    export CC="$TARGET-gcc"
    extract_dependency $LIBGMP

    export CPPFLAGS="-I$DEPENDROOT/usr/include"
    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --prefix="/usr" \
                                     --with-gmp="$DEPENDROOT/usr" \
                                     --enable-static \
                                     --disable-shared \
                                     CPPFLAGS="$CPPFLAGS" \
                                     CFLAGS="$CFLAGS" \
                                     LDFLAGS="$LDFLAGS" \

    ;;

  build)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY/"
    ;;

  build-tools)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

