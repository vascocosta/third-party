##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the zsh shell.
##
## Author:
##
##     Evan Green 31-Aug-2016
##
## Environment:
##
##     Build
##

. ../build_common.sh

PCRE=libpcre_8.39
NCURSES=libncurses_5.9
LIBICONV=libiconv_1.14
LIBGDBM=libgdbm_1.12

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    extract_dependency "$PCRE"
    extract_dependency "$NCURSES"
    extract_dependency "$LIBICONV"
    extract_dependency "$LIBGDBM"
    export CPPFLAGS="$CPPFLAGS -I$DEPENDROOT/usr/include"
    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    export LD_LIBRARY_PATH="$DEPENDROOT/usr/lib"
    export CC="$TARGET-gcc"
    export zsh_cv_sys_path_dev_fd=no
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --enable-pcre \
                                     --with-tcsetpgrp \
                                     --prefix="/usr" \
                                     --sysconfdir=/etc/zsh \
                                     --enable-etcdir=/etc/zsh \
                                     CFLAGS="$CFLAGS" \
                                     CPPFLAGS="$CPPFLAGS" \
                                     LDFLAGS="$LDFLAGS"

    ;;

  build)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

