##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the git utility.
##
## Author:
##
##     Evan Green 16-Apr-2015
##
## Environment:
##
##     Build
##

. ../package_common.sh

[ -n "$PACKAGE_DIRECTORY" ] && rm -rf "$PACKAGE_DIRECTORY/usr"
cp -Rpv "$BUILD_DIRECTORY/usr" "$PACKAGE_DIRECTORY"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: git
Depends: libiconv, curl, expat, less
Priority: optional
Version: 2.3.5
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: https://www.kernel.org/pub/software/scm/git/git-2.3.5.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: Git version control suite.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

