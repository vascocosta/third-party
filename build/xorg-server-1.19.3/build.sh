##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes xorg-server-1.19.3
##
## Author:
##
##     Evan Green 4-Apr-2017
##
## Environment:
##
##     Build
##

. ../build_common.sh
. ../x_common.sh

export PKG_CONFIG="$PKG_CONFIG --dont-define-prefix"
cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    export CC="$TARGET-gcc"
    extract_dependency libopenssl_1.0.2h
    extract_dependency libpixman_0.34.0
    extract_dependency xproto_7.0.31
    extract_dependency util-macros_1.19.1
    extract_dependency libx11_1.6.5
    extract_dependency libxmu_1.1.2
    extract_dependency glproto_1.4.17
    extract_dependency mesa_17.0.0
    extract_dependency libxshmfence_1.2
    extract_dependency libxdmcp_1.1.2
    extract_dependency libxrender_0.9.10
    extract_dependency libnettle_3.2
    extract_dependency libxi_1.7.9
    extract_dependency libxaw_1.0.13
    extract_dependency libxt_1.1.5
    extract_dependency libxpm_3.5.12
    extract_dependency font-util_1.3.1

    extract_dependency kbproto_1.0.7
    extract_dependency xextproto_7.3.0
    extract_dependency fixesproto_5.0
    extract_dependency damageproto_1.2.1
    extract_dependency xcmiscproto_1.2.2
    extract_dependency xtrans_1.3.5
    extract_dependency bigreqsproto_1.1.2
    extract_dependency randrproto_1.5.0
    extract_dependency renderproto_0.11.1
    extract_dependency inputproto_2.3.2
    extract_dependency fontsproto_2.1.3
    extract_dependency videoproto_2.3.3
    extract_dependency compositeproto_0.4.2
    extract_dependency recordproto_1.14.2
    extract_dependency scrnsaverproto_1.2.2
    extract_dependency resourceproto_1.2.0
    extract_dependency presentproto_1.1
    extract_dependency xineramaproto_1.2.1
    extract_dependency libxkbfile_1.0.9
    extract_dependency libxfont2_2.0.1
    extract_dependency xf86dgaproto_2.1
    extract_dependency xf86vidmodeproto_2.3.1
    extract_dependency dmxproto_2.3.1
    extract_dependency dri2proto_2.8
    extract_dependency dri3proto_1.0

    extract_dependency libfontenc_1.1.3
    extract_dependency libxcb_1.12
    extract_dependency libxau_1.0.8
    extract_dependency libxext_1.3.3
    extract_dependency libfreetype_2.7.1
    extract_dependency libxdamage_1.1.4
    extract_dependency libxfixes_5.0.3
    extract_dependency libpng_1.6.28
    extract_dependency expat_2.1.0
    extract_dependency libice_1.0.9
    extract_dependency libsm_1.2.2

    touch -t200001010000 ${SOURCE_DIRECTORY}/configure.ac

    ##
    ## Link against libGL because it uses the initial-exec TLS model, which
    ## means it has to be loaded with the application itself, not dlopened
    ## later. Glibc has a hack where extra space in the initial TLS allocation
    ## can be used by dlopened static-TLS modules. This was deemed brittle, and
    ## gets complicated if threads have been fired up before the dlopen.
    ##

    export LIBS="-lGL"
    sh ${SOURCE_DIRECTORY}/configure $XORG_CONFIG \
                                     --enable-suid-wrapper \
                                     --disable-systemd-logind \
                                     --with-xkb-output=/var/lib/xkb \
                                     --with-shared-memory-dir=/tmp \
                                     --with-fontrootdir='${prefix}/share/fonts/X11' \
                                     --disable-pciaccess \
                                     --disable-int10-module \
                                     --disable-glamor \
                                     --disable-vgahw \
                                     --disable-vbe \
                                     --disable-libdrm \
                                     --disable-dri2 \
                                     --disable-dri3

    ;;

  build)
    rm -f test/sdksyms.c
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

