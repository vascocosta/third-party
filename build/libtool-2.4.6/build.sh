##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the automake packages (all versions).
##
## Author:
##
##     Evan Green 19-May-2016
##
## Environment:
##
##     Build with POSIX tools.
##

. ../build_common.sh

##
## Automake needs autoconf.
##

if test "x$BUILD_OS" = "xwin32"; then

    ##
    ## Unfortunately Perl backtick commands send each word as a separate
    ## argument, so simply setting it to "sh -c" would get expanded to
    ## "sh -c 'word0' '...'", when it really should be "sh -c 'word0 ...'".
    ## This "script" repackages the command line to be in a single argument.
    ##

    export PERL5SHELL='sh -c "sh -c \\"\$*\\"" dummy'
    export M4="$TOOLBINROOT/bin/m4"
    export TMPDIR="$BUILD_DIRECTORY"
    export ac_cv_path_PERL='perl'
fi

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure-tools)
    export AUTOCONF="$TOOLBINROOT/bin/autoconf"
    touch $SOURCE_DIRECTORY/aclocal.m4
    touch $SOURCE_DIRECTORY/configure
    touch $SOURCE_DIRECTORY/Makefile.in
    touch $SOURCE_DIRECTORY/config-h.in
    touch $SOURCE_DIRECTORY/doc/libtool.1
    touch $SOURCE_DIRECTORY/doc/libtool.info
    touch $SOURCE_DIRECTORY/libltdl/aclocal.m4
    touch $SOURCE_DIRECTORY/libltdl/configure
    touch $SOURCE_DIRECTORY/libltdl/Makefile.in
    touch $SOURCE_DIRECTORY/libltdl/config-h.in
    touch $SOURCE_DIRECTORY/doc/stamp-vti
    touch $SOURCE_DIRECTORY/doc/version.texi
    touch $SOURCE_DIRECTORY/doc/libtool.info
    sh ${SOURCE_DIRECTORY}/configure --prefix=/
    ;;

  configure)
    export CC="$TARGET-gcc"
    CFLAGS="$CFLAGS -fno-builtin"
    if test "x$BUILD_OS" = "xwin32"; then
        export ac_cv_func_strchr=yes
        export ac_cv_func_strrchr=yes
        export ac_cv_func_rindex=yes
        export ac_cv_func_memcpy=yes
        export ac_cv_func_bcopy=yes
        export ac_cv_func_memmove=yes
        export ac_cv_func_strcmp=yes
        export ac_cv_func_closedir=yes
        export ac_cv_func_opendir=yes
        export ac_cv_func_readdir=yes
    fi

    extract_dependency "autoconf_2.69"
    export AUTOCONF="$DEPENDROOT/usr/bin/autoconf"
    export AUTOM4TE="$DEPENDROOT/usr/bin/autom4te"
    export PERL5LIB="$DEPENDROOT/usr/share/autoconf"
    sed "s|'/usr/share/autoconf'|'$DEPENDROOT/usr/share/autoconf'|" \
      "$DEPENDROOT/usr/share/autoconf/autom4te.cfg" \
      >"$DEPENDROOT/usr/share/autoconf/autom4te.cfg2"

    export AUTOM4TE_CFG="$DEPENDROOT/usr/share/autoconf/autom4te.cfg2"
    touch $SOURCE_DIRECTORY/aclocal.m4
    touch $SOURCE_DIRECTORY/configure
    touch $SOURCE_DIRECTORY/Makefile.in
    touch $SOURCE_DIRECTORY/config-h.in
    touch $SOURCE_DIRECTORY/doc/libtool.1
    touch $SOURCE_DIRECTORY/doc/libtool.info
    touch $SOURCE_DIRECTORY/libltdl/aclocal.m4
    touch $SOURCE_DIRECTORY/libltdl/configure
    touch $SOURCE_DIRECTORY/libltdl/Makefile.in
    touch $SOURCE_DIRECTORY/libltdl/config-h.in
    touch $SOURCE_DIRECTORY/doc/stamp-vti
    touch $SOURCE_DIRECTORY/doc/version.texi
    touch $SOURCE_DIRECTORY/doc/libtool.info
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --prefix="/usr" \
                                     --disable-static \
                                     CFLAGS="$CFLAGS" \
                                     LDFLAGS="$LDFLAGS"

    ;;

  build | build-tools)
    $MAKE
    $MAKE install DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

