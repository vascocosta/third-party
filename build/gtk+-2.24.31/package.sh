##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the GTK+ framework.
##
## Author:
##
##     Evan Green 19-Apr-2017
##
## Environment:
##
##     Build
##

. ../package_common.sh

##
## Avoid trailing slashes in the cp commands, as cp on Darwin copies the
## contents of the directory (rather than the directory itself) if there are
## trailing slashes on either the source or the destination.
##

rm -rf "$PACKAGE_DIRECTORY/usr"
mkdir -p "$PACKAGE_DIRECTORY/usr"
rm -f $BUILD_DIRECTORY/usr/lib/*.la
rm -f $BUILD_DIRECTORY/usr/lib/gtk-2.0/2.10.0/engines/*.la
rm -f $BUILD_DIRECTORY/usr/lib/gtk-2.0/2.10.0/immodules/*.la
rm -f $BUILD_DIRECTORY/usr/lib/gtk-2.0/2.10.0/printbackends/*.la
rm -f $BUILD_DIRECTORY/usr/lib/gtk-2.0/modules/*.la
rm -rf $BUILD_DIRECTORY/usr/share/man

cp -Rpv "$BUILD_DIRECTORY/usr" "$PACKAGE_DIRECTORY"
cp -Rpv "$BUILD_DIRECTORY/etc" "$PACKAGE_DIRECTORY"

    extract_dependency libatk_2.24.0
    extract_dependency gdk-pixbuf_2.36.6
    extract_dependency libpango_1.40.5
    extract_dependency libx11_1.6.5
    extract_dependency libxinerama_1.1.3
    extract_dependency xinput_1.6.2
    extract_dependency libxi_1.7.9

    extract_dependency libcairo_1.14.8
    extract_dependency libglib_2.49.6
    extract_dependency libpng_1.6.28
    extract_dependency libjpeg-turbo_1.5.1
    extract_dependency tiff_4.0.7
    extract_dependency gettext_0.19.8.1
    extract_dependency libz_1.2.11
    extract_dependency libpcre_8.39
    extract_dependency libpixman_0.34.0
    extract_dependency libxcb_1.12
    extract_dependency libxau_1.0.8
    extract_dependency xproto_7.0.31
    extract_dependency libxrender_0.9.10
    extract_dependency renderproto_0.11.1
    extract_dependency kbproto_1.0.7
    extract_dependency libxext_1.3.3
    extract_dependency xextproto_7.3.0
    extract_dependency libiconv_1.14
    extract_dependency libffi_3.2.1
    extract_dependency libxml2_2.9.4
    extract_dependency libunistring_0.9.6
    extract_dependency xineramaproto_1.2.1
    extract_dependency fontconfig_2.12.1
    extract_dependency libfreetype_2.7.1
    extract_dependency libharfbuzz_1.4.4
    extract_dependency inputproto_2.3.2

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: gtk
Depends: libatk, gdk-pixbuf, libpango, libx11, libxinerama, libxi, libcairo, libpixman, tiff, libgcc
Priority: optional
Version: 2.36.6
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: http://ftp.gnome.org/pub/gnome/sources/gtk+/2.24/gtk+-2.24.31.tar.xz
Installed-Size: $INSTALLED_SIZE
Description: Framework used for creating graphical user interfaces.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

