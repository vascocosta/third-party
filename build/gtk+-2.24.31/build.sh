##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the gtk+ package.
##
## Author:
##
##     Evan Green 19-Apr-2017
##
## Environment:
##
##     Build
##

. ../build_common.sh

export PATH=$PATH:$DEPENDROOT/usr/bin
export LD_LIBRARY_PATH=$DEPENDROOT/usr/lib

export GDK_PIXBUF_MODULEDIR=$DEPENDROOT/usr/lib/gdk-pixbuf-2.0/2.10.0/loaders
export GDK_PIXBUF_MODULE_FILE=$BUILD_DIRECTORY/loaders.cache

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    extract_dependency libatk_2.24.0
    extract_dependency gdk-pixbuf_2.36.6
    extract_dependency libpango_1.40.5
    extract_dependency libx11_1.6.5
    extract_dependency libxinerama_1.1.3
    extract_dependency xinput_1.6.2
    extract_dependency libxi_1.7.9

    extract_dependency libcairo_1.14.8
    extract_dependency libglib_2.49.6
    extract_dependency libpng_1.6.28
    extract_dependency libjpeg-turbo_1.5.1
    extract_dependency tiff_4.0.7
    extract_dependency gettext_0.19.8.1
    extract_dependency libz_1.2.11
    extract_dependency libpcre_8.39
    extract_dependency libpixman_0.34.0
    extract_dependency libxcb_1.12
    extract_dependency libxau_1.0.8
    extract_dependency xproto_7.0.31
    extract_dependency libxrender_0.9.10
    extract_dependency renderproto_0.11.1
    extract_dependency kbproto_1.0.7
    extract_dependency libxext_1.3.3
    extract_dependency xextproto_7.3.0
    extract_dependency libiconv_1.14
    extract_dependency libffi_3.2.1
    extract_dependency libxml2_2.9.4
    extract_dependency libunistring_0.9.6
    extract_dependency xineramaproto_1.2.1
    extract_dependency fontconfig_2.12.1
    extract_dependency libfreetype_2.7.1
    extract_dependency libharfbuzz_1.4.4
    extract_dependency inputproto_2.3.2

    export CFLAGS="$CFLAGS"
    export CPPFLAGS="-I$DEPENDROOT/usr/include"
    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    ##
    ## Create a loaders.cache for gdk-pixbuf.
    ##

    gdk-pixbuf-query-loaders > $GDK_PIXBUF_MODULE_FILE
    export CC="$TARGET-gcc"
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --prefix="/usr" \
                                     --sysconfdir=/etc \
                                     --disable-static \
                                     --with-x \
                                     --with-xinput \
                                     CFLAGS="$CFLAGS" \
                                     LDFLAGS="$LDFLAGS"

    ;;

  build)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

