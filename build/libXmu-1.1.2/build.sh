##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the libXmu-1.1.2 package.
##
## Author:
##
##     Evan Green 22-Mar-2017
##
## Environment:
##
##     Build
##

. ../build_common.sh
. ../x_common.sh

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    export CC="$TARGET-gcc"
    extract_dependency libxau_1.0.8
    extract_dependency libxcb_1.12
    extract_dependency libx11_1.6.5
    extract_dependency xproto_7.0.31
    extract_dependency xextproto_7.3.0
    extract_dependency kbproto_1.0.7
    extract_dependency libxext_1.3.3
    extract_dependency libsm_1.2.2
    extract_dependency libice_1.0.9
    extract_dependency libxt_1.1.5

    sh ${SOURCE_DIRECTORY}/configure $XORG_CONFIG
    ;;

  build)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

