##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes intltool.
##
## Author:
##
##     Evan Green 4-Apr-2017
##
## Environment:
##
##     Build
##

. ../build_common.sh

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    extract_dependency expat_2.1.0

    ##
    ## Use this horrendousness to get XML::Parser installed.
    ##

    incpath=$DEPENDROOT/usr/include
    libpath=$DEPENDROOT/usr/lib
    args="EXPATINCPATH='$incpath' EXPATLIBPATH='$libpath'"
    line="o conf makepl_arg \"$args\""
    PERL_MM_USE_DEFAULT=1 echo | cpan
    printf "$line\n o conf commit" | cpan
    perl -MCPAN -e "CPAN::Shell->rematein('notest', 'install', 'XML::Parser')"

    export CC="$TARGET-gcc"
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --enable-shared \
                                     --with-pic \
                                     --prefix="/usr" \
                                     CFLAGS="$CFLAGS"

    ;;

  build)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

