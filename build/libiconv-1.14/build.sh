##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the iconv library.
##
## Author:
##
##     Evan Green 2-Apr-2015
##
## Environment:
##
##     Build
##

. ../build_common.sh

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure-tools)
    sh ${SOURCE_DIRECTORY}/configure --prefix="$OUTPUT_DIRECTORY"
    ;;

  configure)

    ##
    ## Export some variables needed to build correctly on Windows.
    ##

    if test "x$BUILD_OS" = "xwin32"; then
        export gl_cv_func_working_strerror=yes
        export gl_cv_func_strerror_0_works=yes
    fi

    export CC="$TARGET-gcc"
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --enable-shared \
                                     --with-pic \
                                     --prefix="/usr" \
                                     CFLAGS="$CFLAGS"

    ;;

  build-tools)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install
    ;;

  build)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

