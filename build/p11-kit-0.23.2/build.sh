##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the p11 kit.
##
## Author:
##
##     Evan Green 1-Sep-2016
##
## Environment:
##
##     Build
##

. ../build_common.sh

LIBTASN1=libtasn1_4.9
LIBFFI=libffi_3.2.1
LIBICONV=libiconv_1.14
GETTEXT=gettext_0.19.8.1

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    extract_dependency "$LIBTASN1"
    extract_dependency "$LIBFFI"
    extract_dependency "$LIBICONV"
    extract_dependency "$GETTEXT"

    export CPPFLAGS="$CPPFLAGS -I$DEPENDROOT/usr/include"
    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib -lpthread"

    export CC="$TARGET-gcc"
    export LIBTASN1_CFLAGS=" "
    export LIBTASN1_LIBS="-ltasn1"
    export LIBFFI_CFLAGS=" "
    export LIBFFI_LIBS="-lffi"
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --disable-static \
                                     --disable-rpath \
                                     --prefix="/usr" \
                                     --with-libiconv-prefix="$DEPENDROOT/usr" \
                                     --with-libintl-prefix="$DEPENDROOT/usr" \
                                     --with-trust-paths=/etc/ssl/ca-bundle.crt \
                                     --with-system-config=/etc/pkcs11/pkcs11.conf \
                                     --with-module-config=/etc/pkcs11/modules \
                                     CFLAGS="$CFLAGS" \
                                     CPPFLAGS="$CPPFLAGS" \
                                     LDFLAGS="$LDFLAGS"

    ;;

  build)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

