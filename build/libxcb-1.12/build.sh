##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the libxcb package.
##
## Author:
##
##     Evan Green 22-Mar-2017
##
## Environment:
##
##     Build
##

. ../build_common.sh
. ../x_common.sh

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    export CC="$TARGET-gcc"
    extract_dependency xproto_7.0.31
    extract_dependency util-macros_1.19.1
    extract_dependency xcb-proto_1.12
    extract_dependency libxau_1.0.8

    sh ${SOURCE_DIRECTORY}/configure $XORG_CONFIG \
                                    --enable-xinput \
                                    --without-doxygen \
                                    --docdir='${datadir}/doc/libxcb-1.12'

    ;;

  build)
    overrides="XCBPROTO_XCBINCLUDEDIR=$DEPENDROOT/usr/share/xcb \
 XCBPROTO_XCBPYTHONDIR=$DEPENDROOT/usr/lib/python2.7/site-packages"

    $MAKE $PARALLEL_MAKE $overrides
    $MAKE $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY" $overrides
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

