##
## Copyright (c) 2016 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the Apache web server package.
##
## Author:
##
##     Evan Green 20-Jun-2016
##
## Environment:
##
##     Build
##

. ../build_common.sh

OPENSSL=libopenssl_1.0.2h
APR=libapr_1.5.1
APRUTIL=libaprutil_1.5.4
ZLIB=libz_1.2.11
PCRE=libpcre_8.39
LIBXML2=libxml2_2.9.4
LIBGCC=libgcc_6.3.0

APRLIBDIR="$DEPENDROOT/usr/lib/apr-1"
cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    export CC="$TARGET-gcc"
    extract_dependency "$OPENSSL"
    [ -f /usr/include/apr-1/apr_version.h ] || {
        extract_dependency "$APR"
        WITH_APR=--with-apr="$DEPENDROOT/usr"
    }

    [ -f /usr/include/apr-1/apu_version.h ] || {
        extract_dependency "$APRUTIL"
        WITH_APR_UTIL=--with-apr-util="$DEPENDROOT/usr"
    }

    extract_dependency "$ZLIB"
    extract_dependency "$PCRE"
    extract_dependency "$LIBXML2"
    extract_dependency "$LIBGCC" || echo "Ignoring libgcc."
    export CFLAGS="$CFLAGS"
    export CPPFLAGS="-I$DEPENDROOT/usr/include"
    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    ##
    ## Fix up apr-1-config.
    ##

    sed -e "s|^prefix=.*|prefix=\"$DEPENDROOT/usr\"|" \
        -e "s|^installbuilddir=.*|installbuilddir=\"$APRLIBDIR\"|" \
        "$DEPENDROOT/usr/bin/apr-1-config" > \
        $DEPENDROOT/usr/bin/apr-1-config.tmp

    mv "$DEPENDROOT/usr/bin/apr-1-config.tmp" "$DEPENDROOT/usr/bin/apr-1-config"
    chmod +x "$DEPENDROOT/usr/bin/apr-1-config"

    if test "$BUILD_OS" != "minoca"; then
        export ac_cv_path_PCRE_CONFIG="$DEPENDROOT/usr/bin/pcre-config"

    else
        PCRE_LINE="--with-pcre=$DEPENDROOT/usr"
    fi

    if test "$BUILD_OS" != "minoca"; then
        export ap_cv_void_ptr_lt_long=no
    fi

    ##
    ## Remove libtool nonsense.
    ##

    rm $DEPENDROOT/usr/lib/*.la || echo whatever rm
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --disable-nls \
                                     $WITH_APR \
                                     $WITH_APR_UTIL \
                                     --with-z="$DEPENDROOT/usr/" \
                                     --with-libxml2="$DEPENDROOT/usr/" \
                                     $PCRE_LINE \
                                     --prefix="/usr" \
                                     --enable-proxy \
                                     --enable-ssl \
                                     --enable-layout=RedHat \
                                     --enable-mods-shared="all cgi" \
                                     --enable-mpms-shared=all \
                                     --enable-suexec=shared \
                                     --with-suexec-bin=/usr/lib/apache/suexec \
                                     --with-suexec-caller=apache \
                                     --with-suexec-userdir=public_html \
                                     CFLAGS="$CFLAGS" \
                                     CPPFLAGS="$CPPFLAGS" \
                                     LDFLAGS="$LDFLAGS"

    ##
    ## If cross compiling, compile and run gen_test_char manually.
    ##

    if test "x$BUILD_OS" != "xminoca"; then
        mkdir -p ./server
        mkdir -p ./include
        touch ./server/gen_test_char.lo
        touch ./server/gen_test_char
        gcc -Wall -O2 -g -DCROSS_COMPILE \
            ${SOURCE_DIRECTORY}/server/gen_test_char.c -o ./server/gen_test_char

        ./server/gen_test_char > ./server/test_char.h
    fi

    ;;

  build)
    $MAKE $PARALLEL_MAKE
    $MAKE install DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

