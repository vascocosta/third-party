##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the FFmpeg package.
##
## Author:
##
##     Chris Stevens 19-Jan-2017
##
## Environment:
##
##     Build
##

. ../build_common.sh

LIBX264=libx264_20170102-2245
LIBVORBIS=libvorbis_1.3.5

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    extract_dependency "$LIBX264"
    extract_dependency "$LIBVORBIS"
    if test "x$BUILD_OS" != "xminoca"; then
        CROSS_COMPILE="--enable-cross-compile"
        CROSS_PREFIX="--cross-prefix=$TARGET-"
    fi

    export CFLAGS="$CFLAGS -I$DEPENDROOT/usr/include"
    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib"

    echo "This will appear to do nothing for awhile. Hang tight..."
    sh ${SOURCE_DIRECTORY}/configure $CROSS_COMPILE \
                                     $CROSS_PREFIX \
                                     --arch="$ARCH" \
                                     --host-os="$TARGET" \
                                     --target-os="$TARGET" \
                                     --prefix="/usr" \
                                     --enable-shared \
                                     --disable-static \
                                     --disable-logging \
                                     --disable-doc \
                                     --disable-sdl \
                                     --disable-sdl2 \
                                     --enable-gpl \
                                     --enable-libx264 \
                                     --enable-libvorbis \
                                     --cc="$TARGET-gcc" \
                                     --cxx="$TARGET-g++" \
                                     --extra-cflags="$CFLAGS" \
                                     --extra-ldflags="$LDFLAGS"

    ;;

  build)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

