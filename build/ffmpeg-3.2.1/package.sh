##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the FFmpeg framework.
##
## Author:
##
##     Chris Stevens 19-Jan-2017
##
## Environment:
##
##     Build
##

. ../package_common.sh

##
## Avoid trailing slashes in the cp commands, as cp on Darwin copies the
## contents of the directory (rather than the directory itself) if there are
## trailing slashes on either the source or the destination.
##

cp -Rpv "$BUILD_DIRECTORY/usr" "$PACKAGE_DIRECTORY"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: ffmpeg
Depends: libvorbis, libx264, libogg
Priority: optional
Version: 3.2.1
Architecture: $PACKAGE_ARCH
Maintainer: Chris Stevens <chris@minocacorp.com>
Section: main
Source: https://www.ffmpeg.org/releases/ffmpeg-3.2.1.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: Multimedia framework used to decode, encode, play and stream video.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

