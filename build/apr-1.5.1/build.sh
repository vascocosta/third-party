##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the Apache Runtime Library package.
##
## Author:
##
##     Evan Green 7-Mar-2015
##
## Environment:
##
##     Build
##

. ../build_common.sh

##
## Export some variables needed to cross compile correctly.
##

if test "$BUILD_OS" != "minoca"; then
    export ac_cv_file__dev_zero=yes
    export ac_cv_func_setpgrp_void=yes
    export ac_cv_sizeof_struct_iovec=8
    export ac_cv_strerror_r_rc_int=yes
    export apr_cv_process_shared_works=yes
    export apr_cv_mutex_robust_shared=no

    ##
    ## TODO: Change this to yes if TCP_CORK is added.
    ##

    export apr_cv_tcp_nodelay_with_cork=no
fi

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    export CC="$TARGET-gcc"
    export FORCE_UNSAFE_CONFIGURE=1
    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --prefix="/usr" \
                                     --with-installbuilddir="/usr/lib/apr-1" \
                                     --with-devrandom=/dev/urandom \
                                     CFLAGS="$CFLAGS"

    ##
    ## If cross compiling, compile and run gen_test_char manually.
    ##

    if test "$BUILD_OS" != "minoca"; then
        mkdir -p ./tools
        mkdir -p ./include/private
        touch ./tools/gen_test_char.lo
        touch ./tools/gen_test_char
        gcc -Wall -O2 -g -DCROSS_COMPILE \
            ${SOURCE_DIRECTORY}/tools/gen_test_char.c -o ./tools/gen_test_char

        ./tools/gen_test_char > ./include/private/apr_escape_test_char.h
    fi
    ;;

  build)
    $MAKE $PARALLEL_MAKE
    echo "Now running make install (not in parallel)"
    $MAKE install \
         prefix="$OUTPUT_DIRECTORY" \
         installbuilddir="$OUTPUT_DIRECTORY/lib/apr-1"

    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

