##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the DirectFB library.
##
## Author:
##
##     Evan Green 28-Feb-2017
##
## Environment:
##
##     Build
##

. ../package_common.sh

##
## Avoid trailing slashes in the cp commands, as cp on Darwin copies the
## contents of the directory (rather than the directory itself) if there are
## trailing slashes on either the source or the destination.
##

rm -rf "$PACKAGE_DIRECTORY/usr"
mkdir -p "$PACKAGE_DIRECTORY/etc"
rm -f $BUILD_DIRECTORY/usr/lib/*.la
rm -rf "$BUILD_DIRECTORY/usr/share/man"
cp -Rpv "$BUILD_DIRECTORY/usr" "$PACKAGE_DIRECTORY"

cat >$PACKAGE_DIRECTORY/etc/directfbrc <<_EOS
#debug
#trace
module-dir=/dev/Volume/Volume3/src/x86dbg/obj/third-party/DirectFB-DIRECTFB_1_7_7.build/build.out/usr/lib/directfb-1.7-7
no-sighandler
log-file=/var/log/directfb.log
system=fbdev
fbdev=/dev/Devices/VideoConsole0
_EOS

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: libdirectfb
Depends: libfreetype, libpng, libjpeg-turbo, libgcc
Priority: optional
Version: 1.7-7
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: https://github.com/Distrotech/DirectFB/archive/DIRECTFB_1_7_7.zip
Installed-Size: $INSTALLED_SIZE
Description: Direct frame buffer access library.
_EOS

cat > "$PACKAGE_DIRECTORY/CONTROL/conffiles" <<_EOS
/etc/directfbrc
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

