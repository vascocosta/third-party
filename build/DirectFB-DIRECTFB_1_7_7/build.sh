##
## Copyright (c) 2013 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the tar package.
##
## Author:
##
##     Evan Green 20-Jan-2014
##
## Environment:
##
##     Build
##

. ../build_common.sh

FREETYPE=libfreetype_2.7.1
LIBPNG=libpng_1.6.28
LIBJPEG=libjpeg-turbo_1.5.1

cd $BUILD_DIRECTORY
export PATH=$PATH:$PWD

case $BUILD_COMMAND in
  configure)
    printf "Compiling fluxcomp..."
    g++ -O2 -g $SRCROOT/third-party/build/DirectFB-DIRECTFB_1_7_7/flux/fluxcomp.cpp -o fluxcomp
    echo "Done"
    extract_dependency "$LIBPNG"
    extract_dependency "$FREETYPE"
    extract_dependency "$LIBJPEG"

    chmod +x "${SOURCE_DIRECTORY}/install-sh"
    export CC="$TARGET-gcc"
    export CFLAGS="$CFLAGS -I$DEPENDROOT/usr/include \
 -I$DEPENDROOT/usr/include/freetype2 -Wno-unused-value"

    export LDFLAGS="$LDFLAGS -L$DEPENDROOT/usr/lib \
 -Wl,-rpath-link=$DEPENDROOT/usr/lib -Wl,--no-undefined -lminocaos"


    export CXXFLAGS="$CFLAGS"
    if [ "$BUILD_OS" = "minoca" ]; then
        export LD_LIBRARY_PATH=$DEPENDROOT/usr/lib
    fi

    sh ${SOURCE_DIRECTORY}/configure $BUILD_LINE \
                                     --host="$TARGET" \
                                     --target="$TARGET" \
                                     --prefix="/usr" \
                                     --sysconfdir=/etc \
                                     --enable-fbdev \
                                     --enable-sawman \
                                     --disable-static \
                                     --enable-shared \
                                     --program-prefix= \
                                     --with-gfxdrivers=none \
                                     --with-inputdrivers=keyboard \
                                     --enable-debug --enable-trace \
                                     CFLAGS="$CFLAGS" \
                                     CXXFLAGS="$CXXFLAGS" \
                                     LDFLAGS="$LDFLAGS"

    ;;

  build)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

