##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the SDL library.
##
## Author:
##
##     Evan Green 21-Feb-2017
##
## Environment:
##
##     Build
##

. ../package_common.sh

rm -f $BUILD_DIRECTORY/usr/lib/*.la
rm -rf "$PACKAGE_DIRECTORY/usr"
mkdir -p "$PACKAGE_DIRECTORY"
cp -Rpv "$BUILD_DIRECTORY/usr" "$PACKAGE_DIRECTORY"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: libsdl2
Depends: libx11, libxinerama, libxrender, libxrandr, libxscrnsaver, libxcursor, libxxf86vm, mesa, libice, libgcc
Priority: optional
Version: 2.0.5
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: https://libsdl.org/release/SDL2-2.0.5.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: Simple DirectMedia Layer library.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

