##
## Copyright (c) 2017 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     build.sh <source_dir> <build_dir> <output_dir> <command>
##
## Abstract:
##
##     This script configures and makes the mkfontscale app.
##
## Author:
##
##     Evan Green 4-Apr-2017
##
## Environment:
##
##     Build
##

. ../build_common.sh
. ../x_common.sh

cd $BUILD_DIRECTORY
case $BUILD_COMMAND in
  configure)
    export CC="$TARGET-gcc"
    extract_dependency xproto_7.0.31
    extract_dependency util-macros_1.19.1
    extract_dependency libz_1.2.11
    extract_dependency libfontenc_1.1.3
    extract_dependency libfreetype_2.7.1
    extract_dependency libpng_1.6.28

    sh ${SOURCE_DIRECTORY}/configure $XORG_CONFIG
    ;;

  build)
    $MAKE $PARALLEL_MAKE
    $MAKE $PARALLEL_MAKE install DESTDIR="$OUTPUT_DIRECTORY"
    ;;

  *)
    echo $0: Error: Invalid build command ${BUILD_COMMAND}.
    exit 2
    ;;

esac

