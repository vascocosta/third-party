##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the tar utility.
##
## Author:
##
##     Evan Green 13-Feb-2015
##
## Environment:
##
##     Build
##

. ../package_common.sh

##
## Avoid trailing slashes in the cp commands, as cp on Darwin copies the
## contents of the directory (rather than the directory itself) if there are
## trailing slashes on either the source or the destination.
##

mkdir -p "$PACKAGE_DIRECTORY/usr/bin"
cp -pv "$BUILD_DIRECTORY/usr/bin/tar" "$PACKAGE_DIRECTORY/usr/bin"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: tar
Depends: libiconv, libgcc
Priority: optional
Version: 1.27.1
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: http://ftp.gnu.org/gnu/tar/tar-1.27.1.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: GNU tar (tape archive) utility.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

