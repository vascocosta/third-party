##
## Copyright (c) 2015 Minoca Corp. All Rights Reserved.
##
## Script Name:
##
##     package.sh <build_dir> <package_dir>
##
## Abstract:
##
##     This script packages the dash shell.
##
## Author:
##
##     Evan Green 31-Mar-2015
##
## Environment:
##
##     Build
##

. ../package_common.sh

mkdir -p "$PACKAGE_DIRECTORY/usr/bin"
cp -pv "$BUILD_DIRECTORY/bin/dash" "$PACKAGE_DIRECTORY/usr/bin"

INSTALLED_SIZE=`compute_size $PACKAGE_DIRECTORY`
cat > "$PACKAGE_DIRECTORY/CONTROL/control" <<_EOS
Package: dash
Priority: optional
Version: 0.5.8
Architecture: $PACKAGE_ARCH
Maintainer: Evan Green <evan@minocacorp.com>
Section: main
Source: http://gondor.apana.org.au/~herbert/dash/files/dash-0.5.8.tar.gz
Installed-Size: $INSTALLED_SIZE
Description: Dash is a Bourne-compatible shell.
_EOS

create_package "$PACKAGE_DIRECTORY" "$BUILD_DIRECTORY"

